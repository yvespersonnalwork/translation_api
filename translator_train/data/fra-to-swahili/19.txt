voir	Kuona
prévenir	Kuonya
guérir	Kupona
guérir qq'un	Kuponya
voir	Kuona
prévenir	Kuonya
montrer	Kuonyesha
enseigner	Kufundisha
chauffer	Kupasha moto
faire	Kufanya
arriver	Kufika
déménager	Kuhama
manger	Kula
se tenir	Kusimama
s'arrêter	Kusimama
passer	Kupita
faire mal	Kuuma
faire faire	Kufanyiza
faire atteindre	Kufikisha
faire déménager	Kuhamisha
nourrir	Kulisha
faire manger	Kulisha
faire se tenir	Kusimamisha
arrêter	Kusimamisha
faire passer	Kupitisha
faire du mal	Kuumiza
rire	Kucheka
aller	Kuenda
emprunter	Kukopa
pourrir	Kuoza
aimer	Kupenda
pouvoir	Kuweza
faire rire	Kuchekesha
conduire	Kuendesha
prêter	Kukopesha
fermenter	Kuozesha
faire pourrir	Kuozesha
plaire	Kupendeza
permettre	Kuwezesha
être clair	Kuelea
entrer	Kuingia
être rempli	Kujaa
se perdre	Kupotea
être perdu	Kupotea
être diminué	Kupungua
entendre	Kusikia
se promener	Kutembea
expliquer	Kueleza
faire entrer	Kuingiza
introduire	Kuingiza
remplir	Kujaza
perdre	Kupoteza
diminuer	Kupunguza
écouter	Kusikiliza
promener	Kutembeza
revenir	Kurudi
se réjouir	Kufurahi
savoir	Kufahamu
rendre	Kurudisha
réjouir	Kufurahisha
informer	Kufahamisha
faire savoir	Kufahamisha
se réveiller	Kuamka
tomber	Kuanguka
bouillir	Kuchemka
être fatigué	Kuchoka
obtenir	Kupata
avoir	Kupata
brûler	Kuwaka
réveiller qq'un	Kuamsha
faire tomber	Kuangusha
faire bouillir	Kuchemsha
fatiguer qq'un	Kuchokesha
faire obtenir	Kupasha
allumer le feu	Kuwasha
la chance	Bahati
une certitude	Hakika
lisse	Laini
souple	Laini
propre	Safi
égal	Sawa
prêt	Tayari
tenter sa chance	Kubahatisha
s'assurer de	Kuhakikisha
assouplir	Kulainisha
nettoyer	Kusafisha
mettre en ordre	Kusawazisha
préparer	Kutayarisha
Ne me faites pas mal	Usiniumize
Baisse le prix s'il te plaît	Punguza bei tafadhali
Tes nouvelles me réjouissent beaucoup	Habari zako zinanifurahisha sana
Accueillons nos invités	Tuwakaribishe wageni wetu
Fais-moi savoir si tu es prêt	Nifahamishe kama u tayari
Ce médicament te guérira rapidement	Dawa hili litakuponya haraka
Montre-moi le chemin	Nionyeshe njia
écrire	Ku-andika
s'écrire	Ku-andikiana
être utile	Ku-faa
aller	Ku-faa
se ressembler	Ku-fanana
suivre	Ku-fuata
se suivre	Ku-fuatana
connaître	Ku-jua
savoir	Ku-jua
se connaître	Ku-juana
manquer	Ku-kosa
se quereller	Ku-kosana
rencontrer	Ku-kuta
se rencontrer	Ku-kutana
attendre	Ku-ngoja
s'attendre	Ku-ngojana
se marier à qq'un	Ku-oa
se marier	Ku-oana
obtenir	Ku-pata
se mettre d'accord	Ku-patana
aimer	Ku-penda
s'aimer	Ku-pendana
frapper	Ku-piga
battre	Ku-piga
se battre	Ku-pigana
aider	Ku-saidia
s'entraider	Ku-saidiana
vaincre	Ku-shinda