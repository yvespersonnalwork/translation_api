écrire	Kuandika
laisser	Kuacha
faire	Kufanya
obtenir	Kupata
obtenir	Kupata
passer	Kupita
jeter	Kutupa
écrire pour	Kuandikia
écrire à	Kuandikia
laisser pour	Kuachia
laisser à	Kuachia
faire pour	Kufanyia
obtenir pour	Kupatia
passer par	Kupitia
jeter à	Kutupia
dédaigner	Kudharau
avoir du mépris pour	Kudharaulia
oublier	Kusahau
oublier de	Kusahaulia
dire	Kuambia
entrer	Kuingia
donner	Kupa
demander	Kuuliza
dire à	Kuambia
entrer dans	Kuingia
donner à	Kupa
demander à	Kuuliza
Un couteau pour couper la viande	Kisu cha kukatia nyama
L'argent pour acheter des vêtements	Fedha za kununulia nguo
La salle à manger	Chumba cha kulia
De l'eau pour le bain	Maji ya kuogea
Il a lu le livre	Alisoma kitabu
Il m'a lu le livre	Alinisomea kitabu
Le livre a été lu par lui	Kitabu kilisomwa naye
On m'a lu le livre	Mimi nilisomewa kitabu
J'ai acheté du sucre	Nimenunua sukari
Je leur ai acheté du sucre	Nimewanunulia sukari
Le sucre a été acheté	Sukari imenunuliwa
On leur a acheté du sucre	Wamenunul iwa sukari
Le voleur a volé le sac	Mwizi ameiba mkoba
Le voleur a volé son sac au blanc	Mwizi ameibia mzungu mkoba wake
Le sac a été volé	Mkoba umeibwa
On a volé son sac au blanc	Mzungu ameibiwa mkoba wake
Ecris une lettre	Andika barua
Ecris-lui une lettre	Mwandikie barua
Ecris une lettre à ta mère	Mwandikie  mamako barua
Ouvre la porte	Fungua mlango
Ouvre-leur la porte	Wafungulie mlango
Ouvre la porte aux invités	Wafungulie wageni mlango
Il lit un livre	Anasoma kitabu
Il lit un livre à son copain	Anamsomea mwenzie kitabu
Dis-moi	Niambie
Dis-lui	Mwambie
Dis-le à ton père	Mwambie babako
Dis à ton père	Mwambie babako
Les enfants nous ont chanté des chansons	Watoto walitu imbia nyimbo
Maman nous a fait la cuisine	Mama alitu pikia chakula
Achète-moi de la farine pour faire du pain	Ninunulie unga wa kupikia mkate
élever	Kulea
recevoir	Kupokea
retirer	Kuondoa
élever pour	Kulelea
recevoir pour	Kupokelea
retirer à	Kuondolea
retirer pour	Kuondolea
répondre	Kujibu
revenir	Kurudi
saluer	Kusalimu
pardonner	Kusamehe
répondre pour	Kujibia
répondre à	Kujibia
revenir pour	Kurudia
revenir à	Kurudia
donner le bonjour à	Kusalimia
pardonner à	Kusamehea
manger	Kula
boire	Kunywa
à manger	Kulia
pour manger	Kulia
à boire	Kunywea
pour boire	Kunywea
se réveiller	Kuamka
arriver	Kufika
émigrer	Kuhama
partir	Kuhama
sentir mauvais	Kunuka
maltraiter	Kutenda
employer	Kutuma
envoyer	Kutuma
saluer	Kuamkia
atteindre	Kufikia
immigrer	Kuhamia
sentir bon	Kunukia
bien traiter	Kutendea
utiliser	Kutumia
le profit	Faida
la pitié	Huruma
le chagrin	Huzuni
profiter à	Kufaidia
avoir pitié de	Kuhurumia
se faire du chagrin pour	Kuhuzunia
rire	Ku-cheka
mépriser	Ku-dharau
construire	Ku-jenga
se mettre en colère	Ku-kasirika
se baigner	Ku-oga
applaudir	Ku-piga makofi
photographier	Ku-piga picha
réparer	Ku-tengeneza
signer	Ku-tia sahihi
espérer	Ku-tumaini
tirer	Ku-vuta