1	-MOJA
2	-WILI
3	-TATU
4	-NNE
5	-TANO
6	SITA
7	SABA
8	-NANE
9	TISA
10	KUMI
11	KUMI NA -MOJA
12	KUMI NA -WILI
13	KUMI NA -TATU
14	KUMI NA -NNE
15	KUMI NA -TANO
16	KUMI NA SITA
17	KUMI NA SABA
18	KUMI NA -NANE
19	KUMI NA TISA
20	ISHIRINI
Samedi	JUMAMOSI
Dimanche	JUMAPILI
Lundi	JUMATATU
Mardi	JUMANNE
Mercredi	JUMATANO
Jeudi	ALHAMISI
Vendredi	IJUMAA
Janvier	MWEZI WA KWANZA
Février	MWEZI WA PILI
Mars	MWEZI WA TATU
Avril	MWEZI WA NNE
Mai	MWEZI WA TANO
Juin	MWEZI WA SITA
Juillet	MWEZI WA SABA
Août	MWEZI WA NANE
Septembre	MWEZI WA TISA
Octobre	MWEZI WA KUMI
Novembre	MWEZI WA KUMI NA MOJA
Décembre	MWEZI WA KUMI NA MBILI
après	Baadaye
pas encore	Bado
dans un petit instant	Bado kidogo
un agenda	Buku la tarehe
continuellement	Daima
ensuite	Halafu
aussitôt	Mara moja
souvent	Mara nyingi
plus tôt	Mbele
avant	Mbele
pour toujours	Milele
jusqu'à	Mpaka
dans trois jours	Mtondo
il y a longtemps	Hapo kale
finalement	Hatimaye
hier	Jana
avant-hier	Juzi
l'autre jour	Juzi juzi
le mois dernier	Mwezi uliopita
à la fin	Mwisho
finalement	Mwisho
plus tard	Nyuma
avant	Kabla
calendrier	Kalenda
comme d'habitude	Kama kawaida
un siècle	Karne
entre	Kati kati
au milieu	Kati kati
demain	Kesho
après-demain	Kesho kutwa
tous les jours	Kila siku
et puis	Kisha
d'abord	Kwanza
premièrement	Kwanza
aujourd'hui	Leo
quand	Lini
tôt	Mapema
de temps en temps	Mara kwa mara
à ce moment même	Pale pale
immédiatement	Papo hapo
quelquefois	Pengine
quelle heure	Saa ngapi
maintenant	Sasa
aussitôt	Sasa hivi
tout de suite	Sasa hivi
un jour de fête	Sikukuu
toujours	Sikuzote
depuis	Tangu
une date	Tarehe
à quel moment	Wakati gani
la semaine dernière	Wiki iliyopita
autrefois	Zamani