import sys
import io

DATA_PATH=sys.argv[1]
lines = io.open(DATA_PATH, 'rt', encoding='utf8').read().split('\n')
content=""
for line in lines[: len(lines)-1]:
    input_text, target_text = line.split('=')
    print(input_text.strip() + '\t' + target_text.strip())
    content=   content + input_text.strip() + '\t' + target_text.strip() + "\n"

f = io.open("temp", "+w")
f.write(content)
