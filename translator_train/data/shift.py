import sys
import io

DATA_PATH=sys.argv[1]
lines = io.open(DATA_PATH, 'rt', encoding='utf8').read().split('\n')
content=""

for line in lines[: len(lines)-1]:
    input_text, target_text = line.split('=')
    print(target_text.strip() + '\t' + input_text.strip())
    content=   content + target_text.strip() + '\t' + input_text.strip() + "\n"


f = io.open(DATA_PATH +".txt", "+w")
f.write(content)
