import sys
import io
#from keras.models import Model
#from keras.callbacks import ModelCheckpoint
#from keras.layers.recurrent import LSTM
#from keras.layers import Dense, Input, Embedding
#from keras.preprocessing.sequence import pad_sequences
#from collections import Counter
#import nltk
#import numpy as np
NUM_SAMPLES = 10000000
#DATA_PATH = 'data/fra-to-lingala.txt'
DATA_PATH = sys.argv[1]

lines = io.open(DATA_PATH, 'rt', encoding='utf8').read().split('\n')
idx=0
for line in lines[: min(NUM_SAMPLES, len(lines)-1)]:
    split_list = line.split('\t')
    idx = idx + 1
    if len(split_list) != 2:
        raise ValueError("Line {}: '{}' has {} spaces, expected 1"
                         .format(idx, line.rstrip(), len(split_list) - 1))
