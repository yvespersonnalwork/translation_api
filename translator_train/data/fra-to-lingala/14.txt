à	na
abeille	nzói
abondant	ya míngi
d'abord	yambo
abord	yambo
absolution	bolimbisi
abstinence	bokili
agaçant	ya kopésa kándá
agréable	elengi
aieul	nkôko
aigre	ngài
aigu	sónge
aiguille	ntonga
aile	lipapû
ailleurs	esíká esúsu
aîné	motúmôlô
aîné	kulútu
ainsi	boye
ainsi	bóngó
air	mopepe
aisance	kabine
lieu d'aisance	kabine
albinos	ndúndu
aliment	bilei
allégresse	nsâi
allumette	fofôlo
allumette	alimeti
alors	na yangó
alors que	nzóka
amabilité	bobóto
accordéon	lindánda
accoucheuse	mwásí-mobôtisi
achat	bosómbi
actuellement	sikawa
acuité	bopôtû
adolescence	bolenge
adolescent	elenge
adulte	mokóló
adultère	pite
adultère	ekóbo
adultère	bokâli
adversaire	mongúná
affaire	likambo
affliction	mawa monene
âme	molimo
amertume	bololo
amertume	pási na motema
ami	moningá
ami	ndéko
à l'amiable	na bobóto
amitié	bondeko
amollir	-lembisa
amont	likoló
amour	bolingo
amulette	talizmá
amulette	nkísi
amusement	lisano
an	mobû
ananas	ananási
ancêtres	bankóko
ancien	ya kala
anciennement	kala kala
ancre	lóngo
âne	mpúnda
ange	mwánje
ange	ánzelú
angle	litúmu
angoisse	nsômo ya bobángi
animal	nyama
anneau	lopete
année	mbúla
annonce	liyébisi
antécédement	yambo penza
antenne	liséké
antérieur	ya libosó-libosó
antérieurement	yambo
antilope	mbólókó
apostat	mopengwi
apôtre	apóstolo
apparence	ekeko
apprenti	moyekoli
après	nsima
après-demain	sima ya lobi
à présent	sikàwa
après-midi	sima ya nzángá
arachide	ngúba
arbre	nzeté
arc	litímbo
arc-en-ciel	monama
argent	mbóngo
armée	mampingá
armoire	etándaká
aubergine	mosángó
aucun	mókó te
aucunement	sókí moke te
aucuns	ba móko
d'aucuns	basúsu
au-dessous	o nse
au-dessus	o likoló
aujourd'hui	leló
auparavant	yambo
auprès de	penepene na
aussi	mpe
aussitôt	mbala moko
autre	esúsu
autrefois	kala kala
aval	ngêle
avant	libosó
avantage	litómbá
avant-hier	ndele
avant-midi	ntóngó
avare	moyími
avec	mpe
avenir	sima ya mikolo
aventurier	waya-wáya
aversion	mpi
avertissement	likebisi
aveugle	moto akúfá míso
