homme d’âge mûr	papa mobimba
homme adulte confirmé	papa mobimba
homme plus du tout jeune	papa mobimba
peine perdue	pasi ya pamba
donne-moi en argent	pesa nga ya kokawuka
donne-moi l’équivalent en liquide	pesa nga ya kokawuka
pourquoi	po na nini
pourquoi ne pas	po na nini te
pourquoi pas	po na nini te
choléra	pulupulu ya makila
félicitations pour l’habillement	putulu
vive le bal Que la fête commence On s’éclate	putulu emata 
fais comme tu veux	sala ndenge olingi
fais comme tu le sens	sala ndenge olingi
fais comme bon te semble	sala ndenge olingi
fais vite	sala noki
dépêche-toi	sala noki
fais ce que tu veux	sala oyo olingi
fais à ta guise	sala oyo olingi
agis à ta guise	sala oyo olingi
échangisme sexuel	sangisa sangisa
libertinage	sangisa sangisa
pratique de partenaires multiples	sangisa sangisa
quelles sont les nouvelles	sango boni
la nouvelle s’est répandue, notre chef s’est enfui	sango epanzani, mokonzi na biso akimi
ça va	sango nini
comment allez-vous	sango nini
quelles sont les nouvelles	sango nini
quoi de neuf	sango nini
personne qui s’incruste chez les autres	semeki kinsekwa
parasite	semeki kinsekwa
mon beau-frère à moi	semeki ya moto
ma belle-sœur à moi	semeki ya moto
mon chéri à moi, de mon cœur	sheli ya moto
ma chérie à moi, de mon cœur	sheli ya moto
tout de suite	sika sikoyo
dans l’immédiat	sika sikoyo
sur le champ	sika sikoyo
maintenant	sikoyo
à l’instant	sikoyo
d’ici peu	sima mua tango
dans un instant	sima mua tango
après qu’il soit	sima na ye ko
après qu’il ait	sima na ye ko
après qu’elle ait	sima na ye ko
après qu’elle soit	sima na ye ko
après avoir	sima ya ko
après être	sima ya ko
après quelques jours	sima ya mua mikolo
au bout de quelques jours	sima ya mua mikolo
après-midi	sima ya nzanga
va-t-en fous le camp tu peux t’en aller	simba nzela, kende
il y a retournement de situation	sis ebongwani nef
la roue a tourné	sis ebongwani nef
la situation s’est inversée	sis ebongwani nef
nullement	soki moke te
en aucun cas	soki moke te
achète ta propre maison	somba oyo ya yo ndaku
coin de l’œil	songe ya lisu
divers	songolo mpakala
différents	songolo mpakala
l’un ou l’autre	songolo mpakala
tel ou tel	songolo mpakala
qui que ce soit	songolo mpakala
quelque soit	songolo mpakala
personne qui respire l’innocence	soso pembe
personne naïve	soso pembe
c’est le top des	suka na
extraordinaire	suka na
merveilleux	suka na
en dernier lieu	suka na suka
au bout du compte	suka na suka
regarde-moi ce Songolo mais pour qui il se prend celui-là	tala ba Songolo misusu
mais c’est de la provocation	tala batu misusu
mais tu me cherches ou quoi	tala batu misusu
regarde tous ces vêtements qu’elle m’a laissés	tala ebele ya bilamba atikeli ngayi
regarde tous ces vêtements qu’il m’a laissés	tala ebele ya bilamba atikeli ngayi
t’as vu ça	tala kaka
n’est-ce pas vrai	tala kaka
c’est aussi ton constat	tala kaka
t’es d’accord avec moi	tala kaka
mais regarde-le mais tu t’es vu tu ne représentes rien	tala ye kuna
ça fait combien	talo boni
le moment est proche	tango ebelemi
il est temps	tango ekoti
le moment est venu	tango ekoti
parfois	tango mosusu
des fois	tango mosusu
toujours	tango nionso
tout le temps	tango nionso
l‘ancien temps	tango ya bakoko
à l’époque des flamands	tango ya banoko
du temps de la colonisation belge	tango ya banoko
homme d’âge mûr	tata mobimba
homme adulte confirmé	tata mobimba
homme plus du tout jeune	tata mobimba
en confrontation	tembe na tembe
par opposition	tembe na tembe
du calme	tia na se
calme-toi	tia na se
on se calme	tia na se
arrête, je ne te crois pas 	tika koloba boye
arrête, tu me fais marcher	tika koloba boye
laisse-moi tranquille	tika ngai kimia
jusqu’à la fin des temps	tii na lifobo
jusqu’au bout	tii na lifobo
jusqu’à épuisement	tii na lifobo
jusqu’à la fin du monde	tii suka molongo
allons-nous en	todie
tout bien considéré, tout compte fait	tokende epayi, tozonga epayi
partons ensemble	tokei nzela moto
allons-nous en ensemble	tokei nzela moto
on se voit un autre jour à une autre fois	tokomi mokolo mosusu
nous sommes proches de la date butoir	tokomi na mikolo ya kotanga
passons à la suite	toleka
allons-y c’est à vous	toleka
parlons peu, parlons bien	toloba epayi, tozonga epayi
peu importe ce que nous disons	toloba toloba te
quoi que nous disions	toloba toloba te
friperies	tombola buaka
le soleil se lève	tongo etani
le jour se lève	tongo etani
nuit blanche	tongo sa