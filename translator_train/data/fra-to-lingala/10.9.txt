le village	mboka
la rue	balabala
l'église	eklézya
le temple	ndako-Nzambe
l'hôtel	lotéle
hôtel	otéle
le cuisinier	molambi
bar	bar
le café	bar
le serveur	mosaleli ya mobali
la serveuse	mosaleli ya mwasi
l'école	etéyelo
l'écolier	muana ya kelasi
étudiant	motangí
étudiants	batangí
l'enseignant	molakisi
le professeur	motéyi
la classe	kelasi
l'Université	kelasi ya likolo
livre	búku
livres	babúku
la bibliothèque	bibilyoteke
le bibliothécaire	mobombi múku
la librairie	magazíni ya búku
le libraire	moteki búku
le vendeur	motéki
le marché	zando
la boulangerie	bulanzelí
le boulanger	mosali-mapa
la boulangère	mosali-mapa
le coiffeur	mokati-nsúki
la coiffeuse	mokati-nsúki
le théâtre	teyatele
le cinéma	sinema
le musée	ndako ya ntoki
la secrétaire	sekretére
le directeur	direktéle
le chèque	se*ki
le chèquier	sekyé
la pièce	likuta
l'argent	mbongo
la prostituée	ndúmba
le voleur	moyibi
le policier	polísi
la prison	bolôko
le cimetière	malíta
la tombe	lilíta
un village	mboka
une rue	balabala
l'église	eklézya
un temple	ndako-Nzambe
l'hôtel	lotéle
hôtel	otéle
un cuisinier	molambi
bar	bar
un café	bar
un serveur	mosaleli ya mobali
une serveuse	mosaleli ya mwasi
l'école	etéyelo
l'écolier	muana ya kelasi
étudiant	motangí
étudiants	batangí
l'enseignant	molakisi
un professeur	motéyi
une classe	kelasi
l'Université	kelasi ya likolo
livre	búku
livres	babúku
une bibliothèque	bibilyoteke
un bibliothécaire	mobombi múku
une librairie	magazíni ya búku
un libraire	moteki búku
un vendeur	motéki
un marché	zando
une boulangerie	bulanzelí
un boulanger	mosali-mapa
une boulangère	mosali-mapa
un coiffeur	mokati-nsúki
une coiffeuse	mokati-nsúki
un théâtre	teyatele
un cinéma	sinema
un musée	ndako ya ntoki
une secrétaire	sekretére
un directeur	direktéle
un chèque	se*ki
un chèquier	sekyé
une pièce	likuta
l'argent	mbongo
une prostituée	ndúmba
un voleur	moyibi
un policier	polísi
une prison	bolôko
un cimetière	malíta
une tombe	lilíta
village	mboka
rue	balabala
église	eklézya
temple	ndako-Nzambe
hôtel	lotéle
hôtel	otéle
cuisinier	molambi
bar	bar
café	bar
serveur	mosaleli ya mobali
serveuse	mosaleli ya mwasi
école	etéyelo
écolier	muana ya kelasi
étudiant	motangí
étudiants	batangí
enseignant	molakisi
professeur	motéyi
classe	kelasi
Université	kelasi ya likolo
livre	búku
livres	babúku
bibliothèque	bibilyoteke
bibliothécaire	mobombi múku
librairie	magazíni ya búku
libraire	moteki búku
vendeur	motéki
marché	zando
boulangerie	bulanzelí
boulanger	mosali-mapa
boulangère	mosali-mapa
coiffeur	mokati-nsúki
coiffeuse	mokati-nsúki
théâtre	teyatele
cinéma	sinema
musée	ndako ya ntoki
secrétaire	sekretére
directeur	direktéle
chèque	se*ki
chèquier	sekyé
pièce	likuta
argent	mbongo
prostituée	ndúmba
voleur	moyibi
policier	polísi
prison	bolôko
cimetière	malíta