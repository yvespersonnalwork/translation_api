mauvaise foi	motema mabe
hypocrisie	motema mabe
sentiment poussant à empêcher la réussite d’autrui	motema mabe
surexcitation d’impatience	motema moto moto
sincérité	motema pembe
sérénité	motema piyo
patience	motema piyo
le cœur pur	motema polele
en toute sincérité	motema polele
sans arrière-pensée	motema polele
sérénité	motema ya kokita
patience	motema ya kokita
âme malfaisante	motema ya likunia
envieux	motema ya likunia
ma chère	moto
mon cher	moto
que ça chauffe que la bagarre commence	moto epela
ça a bardé	moto epeli
sueur froide	motoki ya mobesu
mon cher ami, ma chère amie	moto na ngayi
ma chère amie	moto na ngayi
personne sans foi ni loi	motu bakata
chien fou	motu bakata
le soleil se lève	moyi ebimi
obéissant	muana bitinda
enfant docile	muana bitinda
domestique	muana bitinda
personne chargée d’accomplir une mission	muana etinda
envoyé spécial	muana etinda
ancien séminariste	muana likindo
compatriote	muana mboka
citoyen	muana mboka
enfant voyou	muana mutu bakata
enfant turbulent	muana mutu pasi
enfant unique	muana na likinda
personne sortie de nulle part	muana ndeke
enfant sorti de l’errance	muana ndeke
enfant adopté	muana ndeke
poussin	muana nsusu
gigolo	muana nsusu
voila une jolie femme	muasi alali awa
c’est vraiment une belle femme	muasi alali awa
femme devenue stérile suite à des avortements provoqués	muasi amela milangi
femme en formes	muasi atonda
femme bien en chair	muasi atonda
femme assumant des responsabilités d’homme	muasi mobali
gloire à la femme ayant des formes	muasi muasi nzoto
femme mannequin	muasi ya kataloge
jolie femme	muasi ya kataloge
femme de valeur	muasi ya kilo
femme d’influence	muasi ya kilo
femme d’une position sociale élevée	muasi ya kilo
prostituée	muasi ya ndumba
femme publique	muasi ya ndumba
extraordinaire	muaye te
irréprochable	muaye te
célébrité	mutu akenda sango
personne de grande renommée	mutu akenda sango
l‘homme propose, Dieu dispose	mutu akokana, Nzambe akosukisa
pousse-pousseur	mutua pusu
personne célèbre	mutu asala kombo
personne de grande renommée	mutu asala kombo
vedette	mutu ayebana
quelqu’un de connu	mutu ayebana
personne	mutu te
nul	mutu te
aucun	mutu te
aucune personne	mutu te
personne	moko te
nul	moko te
aucun	moko te
aucune personne	moko te
le noir	mutu moyindo
homme noir	mutu muindu
chaque personne	mutu na mutu
chacun	mutu na mutu
quelqu’un de très proche	mutu ya kati
personne intime	mutu ya kati
fou	mutu ya liboma
bègue	mutu ya likukuma
personne avec un gros ventre	mutu ya libumu monene
aveugle	mutu ya lolanda
personne nuisible	mutu ya misala mabe
personne maléfique	mutu ya misala mabe
aveugle	mutu ya misu ekufa
personne malvoyante	mutu ya misu ekufa
personne impuissante	mutu ya mokongo ekufa
personne de petite taille	mutu ya mokuse
personne de grande taille	mutu ya molayi
personnage maléfique	mutu ya motema mabe
personne chargée de pousser ou de tirer un pousse-pousse	mutu ya pusu
pousse-pousseur	mutu ya pusu
timide	mutu ya soni
en bref	na bokuse
en résumé	na bokuse
à huis clos	na bokutu
en cachette	na bokutu
clandestinement	na bokutu
en secret	na bokutu
à la dérobée	na bokutu
à l’insu	na bokutu
à l’insu de	na bokutu
en longueur	na bolayi
en long et en large	na bolayi
tout au long de	na bolayi
sciemment	na kanaye
expressément méchant	na kanaye
avec de la mauvaise foi	na kanaye
en cachette	na kinsueki
en catimini	na kinsueki
en privé	na kuzu