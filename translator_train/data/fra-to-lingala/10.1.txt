se marier vierge	kobala mobimba
se marier vierge	kobala ngondo
se mettre à	kobanda ko
garder son sang-froid	kobatela kimia
rester calme	kobatela kimia
se retirer	kobenda nzoto
s’en aller	kobenda nzoto
abdiquer	kobenda nzoto
tirer en longueur	kobenda molayi
faire traîner les choses	kobenda molayi
tirer les oreilles à quelqu’un	kobenda mutu matoyi
prévenir quelqu’un	kobenda mutu matoyi
faire une grimace	kobebisa elongi
bouder	kobebisa elongi
faire la gueule	kobebisa elongi
punir	kobeta fimbu
fouetter	kobeta fimbu
parier	kobeta gazon
ignorer	kobeta kara
faire la sourde oreille	kobeta kara
jouer l‘indifférence	kobeta kara
esquiver	kobeta kara
travailler	kobeta kayu
bosser	kobeta kayu
trébucher	kobeta libaku
faire un faux pas	kobeta libaku
faire une erreur	kobeta libaku
avoir une chance insolente	kobeta libaku na zelo
rencontrer la chance	kobeta libaku na zelo
faire une mauvaise rencontre	kobeta libaku ya mabe
être victime d’un coup dur	kobeta libaku ya mabe
tirer à côté	kobeta libanda
commettre une grave erreur	kobeta libanda
être à côté de la plaque	kobeta libanda
faire son boulot	kobeta libanga
travailler	kobeta libanga
raconter	kobeta lisolo
raconter une histoire	kobeta lisolo
narrer une histoire	kobeta lisolo
dire une connerie	kobeta mabe
faire une bourde	kobeta mabe
tirer à côté	kobeta mabe
applaudir	kobeta maboko
marcher	kobeta makolo
se déplacer à pieds	kobeta makolo
tchatcher	kobeta masolo
bavarder	kobeta masolo
parler en privé	kobeta masolo ya kati
recommander ou faire la commission auprès de quelqu’un	kobeta match epayi ya mutu
gifler	kobeta mbata
lancer un cri d’alarme	kobeta mbela
avertir	kobeta mbela
dégager une flatulence	kobeta mokinza
péter	kobeta mokinza
parier	kobeta mondenge
bégayer	kobeta monoko
s’habiller élégamment	kobeta monzele
s’habiller avec classe	kobeta monzele
trahir	kobeta mosinzo
abuser de la confiance de quelqu’un	kobeta mosinzo
donner un coup dans le dos	kobeta mosinzo
faire la sieste	kobeta ngoss
piquer un somme	kobeta ngoss
claironner	kobeta ntangua
faire retentir	kobeta ntangua
donner l’alerte	kobeta ntangua
sonner l’alarme	kobeta ntangua
rater sa cible de justesse	kobeta poto
manquer son coup	kobeta poto
marteler	kobeta sete
insister	kobeta sete
raconter des bobards	kobeta skit
mentir	kobeta skit
bomber le torse	kobeta tolo
se vanter	kobeta tolo
se pavaner	kobeta tolo
esquiver	kobeta tshangi
informer	kobetela masolo
mettre au courant	kobetela masolo
rapporter	kobetela masolo
donner un coup de fil à quelqu’un	kobetela mutu singa
téléphoner à quelqu’un	kobetela mutu singa
provoquer un faux pas	kobetisa libaku
induire en erreur	kobetisa libaku
prononcer une parole déplacée	kobetisa monoko libaku
faire un lapsus linguae	kobetisa monoko libaku
vivre difficilement	kobika na nkokoso
vivre aux dépens de quelqu’un	kobika na sima ya mutu
se classer dernier	kobima lesheke
échouer	kobima lesheke
se ridiculiser	kobima zoba
être prise pour une idiote	kobima zoba
être le dindon de la farce	kobima zoba
faire les gros yeux	kobimisa mbuma miso
dominer quelqu’un	kobinela mutu likolo ya zolo
moquer de quelqu’un	kobinela mutu likolo ya zolo
faire capoter une affaire	koboma masolo
mettre un terme à une négociation	koboma masolo
temporiser	koboma moto
mettre une sourdine à	koboma moto
calmer le jeu	koboma moto
éteindre le feu	koboma moto
contenter	koboma na elengi
donner du plaisir	koboma na elengi
faire jouir	koboma na elengi
se donner à fond	koboma nzoto
se donner de la peine	koboma nzoto
travailler dur	koboma nzoto
glander	koboma tango
corriger son langage	kobongisa maloba
rectifier son discours	kobongisa maloba
changer d’avis	kobongola makanisi
créer des histoires	kobota makambo
engendrer des problèmes	kobota matata
occasionner des ennuis	kobota matata
faire une dédicace pour quelqu’un	kobuaka libanga
jeter de l’eau	kobuaka mayi
éjaculer	kobuaka mayi
demander l’asile politique	kobuaka nzoto
bénir par un parent	kobuaka soyi
bénir	kobuaka soyi
donner des ondes de chance	kobuaka soyi
avorter	kobuaka zemi
provoquer un avortement	kobuaka zemi
mépriser quelqu’un	kobude mutu
ignorer quelqu’un	kobude mutu