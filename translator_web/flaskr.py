from flask import Flask, request, send_from_directory, redirect, render_template, flash, url_for, jsonify, \
    make_response, abort
from flask_cors import CORS, cross_origin

from fra_to_lingala_word_translator_predict import FraToLingalaWordTranslator
from fra_to_swahili_word_translator_predict import FraToSwahiliWordTranslator

from translator_samples.fra_to_swahili_word_search import FraToSwahiliWordSearch
from translator_samples.fra_to_lingala_word_search import FraToLingalaWordSearch

from translator_error.fra_to_swahili_word_error import FraToSwahiliWordError
from translator_error.fra_to_lingala_word_error import FraToLingalaWordError

app = Flask(__name__)
app.config.from_object(__name__)  # load config from this file , flaskr.py
CORS(app, support_credentials=True)

fra_to_lingala_translator_w = FraToLingalaWordTranslator()
fra_to_swahili_translator_w = FraToSwahiliWordTranslator()

fra_to_lingala_search_w = FraToLingalaWordSearch()
fra_to_swahili_search_w = FraToSwahiliWordSearch()

fra_to_lingala_error_w = FraToLingalaWordError()
fra_to_swahili_error_w = FraToSwahiliWordError()

@app.route('/')
def home():
    return render_template('home.html')


@app.route('/about')
def about():
    return 'About Us'

@app.route('/translate_fra', methods=['POST', 'GET'])
@cross_origin(origin='*',headers=['Content- Type','Authorization'])
def translate_fra():
    if request.method == 'POST':
        if not request.json or 'sentence' not in request.json or 'level' not in request.json or 'target_lang' not in request.json:
            abort(400)
        sentence = request.json['sentence']
        level = request.json['level']
        target_lang = request.json['target_lang']
    else:
        sentence = request.args.get('sentence')
        level = request.args.get('level')
        target_lang = request.args.get('target_lang')

    target_text = sentence

    if level == 'word' and target_lang == 'lingala':
        target_text = fra_to_lingala_translator_w.translate_lang(sentence)
         
    elif level == 'word' and target_lang == 'swahili':
        target_text = fra_to_swahili_translator_w.translate_lang(sentence)

    response= jsonify({
        'sentence': sentence,
        'translated': target_text,
        'target_lang': target_lang,
        'level': level
    })
    response.headers.add('Content-Type', 'application/json')
    response.headers.add('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Expose-Headers', 'Content-Type,Content-Length,Authorization,X-Pagination')
    return response


@app.route('/search_fra', methods=['POST', 'GET'])
@cross_origin(origin='*',headers=['Content- Type','Authorization'])
def search_fra():
    if request.method == 'POST':
        if not request.json or 'sentence' not in request.json or 'level' not in request.json or 'target_lang' not in request.json:
            abort(400)
        sentence = request.json['sentence']
        level = request.json['level']
        target_lang = request.json['target_lang']
    else:
        sentence = request.args.get('sentence')
        level = request.args.get('level')
        target_lang = request.args.get('target_lang')

    target_text = sentence

    if level == 'word' and target_lang == 'lingala':
        target_text = fra_to_lingala_search_w.search_lang(sentence)
        
    elif level == 'word' and target_lang == 'swahili':
        target_text = fra_to_swahili_search_w.search_lang(sentence)

    response= jsonify({
        'sentence': sentence,
        'samples': target_text,
        'target_lang': target_lang,
        'level': level
    })
    response.headers.add('Content-Type', 'application/json')
    response.headers.add('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Expose-Headers', 'Content-Type,Content-Length,Authorization,X-Pagination')
    return response

@app.route('/error_fra', methods=['POST', 'GET'])
@cross_origin(origin='*',headers=['Content- Type','Authorization'])
def error_fra():
    if request.method == 'POST':
        if not request.json or 'sentence' not in request.json or 'level' not in request.json or 'target_lang' not in request.json:
            abort(400)
        sentence = request.json['sentence']
        level = request.json['level']
        target_lang = request.json['target_lang']
        translated = request.json['translated']

    else:
        sentence = request.args.get('sentence')
        level = request.args.get('level')
        target_lang = request.args.get('target_lang')
        translated = request.args.get('translated')

    target_text = sentence

    if level == 'word' and target_lang == 'lingala':
        target_text = fra_to_lingala_error_w.error_lang(sentence, translated)
        
    elif level == 'word' and target_lang == 'swahili':
        target_text = fra_to_swahili_error_w.error_lang(sentence, translated)

    response= jsonify({
        'reported': target_text,
    })
    response.headers.add('Content-Type', 'application/json')
    response.headers.add('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Expose-Headers', 'Content-Type,Content-Length,Authorization,X-Pagination')
    return response

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


def main():
    
    #app.run(ssl_context='adhoc', debug=True, host='0.0.0.0')
    app.run(debug=True, host='0.0.0.0')


if __name__ == '__main__':
    main()
