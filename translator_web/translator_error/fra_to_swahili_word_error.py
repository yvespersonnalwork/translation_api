import re
import glob
import json


class FraToSwahiliWordError(object):

	def __init__(self):
		self.attribut = None
		#self.DATA_PATH = '/home/ubuntu/translation_api/translator_train/data/fra-to-lingala/*.txt'

	def error_lang(self, input_text, output_text):
		name = "translator_error/fra_to_swahili_error.txt"

		f = open(name, 'w+', encoding='utf8')
		f.write(str(input_text) + "===" + str(output_text)+ "\n")
		f.close()
		return True

def main():
	#model = FraToLingalaWordError()
	print("")

if __name__ == '__main__':
	main()
