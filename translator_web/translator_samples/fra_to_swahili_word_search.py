import re
import glob
import json


class FraToSwahiliWordSearch(object):

	def __init__(self):
		self.attribut = None
		self.DATA_PATH = '/home/ubuntu/translation_api/translator_train/data/fra-to-swahili/*.txt'

	def search_lang(self, input_text):
		files = glob.glob(self.DATA_PATH)
		responses = []
		words = []
		input_words = self.truncate_sentences(input_text.lower())
		patterns = []
		for word in input_words:
			if (word != " " and word != ""):
				words.append(word)
				patterns.append(re.compile(r'\b({0})\b'.format(word), re.IGNORECASE))
		errors = []
		linenum = 0
		response = "<table style='width:100%'>"
		for name in files:
			with open(name, 'rt', encoding='utf8') as file:
				for line in file:
					line = line.lower()
					linenum += 1
					for i in range(0, len(patterns)):
						if patterns[i].search(line.split("\t")[0]) != None:
							str_text = line.rstrip('\n')
							str_text = str_text.replace("\t", " ==> ")
							str_text = str_text.replace(words[i], "<span style='color: #FFD700'>" + words[i] + "</span>")

							errors.append((linenum, str_text))

			file.close()
		for i in range(len(errors)):
			if i%2 == 0:
				response = response + "<tr bgcolor='#FAFAFA'><td><p>" + str(errors[i][1]) + "</p></td></tr>"
			else:
				response = response + "<tr bgcolor='#FDFDFD'><td><p>" + str(errors[i][1]) + "</p></td></tr>"

		return str(response + "</table>")

	def truncate_sentences(self, input_text):
		response = input_text
		words = input_text.split(' ')
		if len(words):
			response = words
		else:
			response = [i for i in words if len(i) > 2]
		return response


def main():
	model = FraToSwahiliWordSearch()
	print(model.search_lang("puiser"))

if __name__ == '__main__':
	main()
