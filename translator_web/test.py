from flask import Flask, request, send_from_directory, redirect, render_template, flash, url_for, jsonify, \
    make_response, abort
from flask_cors import CORS, cross_origin


app = Flask(__name__)
app.config.from_object(__name__)  # load config from this file , flaskr.py
CORS(app, support_credentials=True)


# Load default config and override config from an environment variable
#app.config.from_envvar('FLASKR_SETTINGS', silent=True)
#app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024




@app.route('/')
def home():
    return render_template('home.html')


@app.route('/about')
def about():
    return 'About Us'

@app.route('/translate_fra', methods=['POST', 'GET'])
@cross_origin(origin='*',headers=['Content- Type','Authorization'])
def translate_fra():
    
    response= jsonify({
        'sentence': "test",
        'translated': "test",
        'target_lang': "target_lang",
        'level': "level"
    })
    response.headers.add('Content-Type', 'application/json')
    response.headers.add('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Expose-Headers', 'Content-Type,Content-Length,Authorization,X-Pagination')
    return response

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


def main():
    
    #app.run(ssl_context='adhoc', debug=True, host='0.0.0.0')
    app.run(debug=True, host='0.0.0.0')


if __name__ == '__main__':
    main()
